#install.packages("tesseract")

# Carregando pacotes
library(tidyverse)
library(tesseract)

ocr("data/hello.png")
ocr("data/hello_manuscript.png")
ocr("data/hello_manuscript.png", engine = tesseract("por"))
ocr("data/text_test_ocr.png")
ocr("data/text_test_ocr_by_hand.jpeg")

# Baixando training data para português
#tesseract_download("por", datapath = NULL, progress = interactive())

ocr("data/frase-motivacional-2.jpg")
ocr("data/frase_pt_1.jpg", engine = tesseract("por"))
ocr("data/texto_pt_sei_escrever.jpeg", engine = tesseract("por"))




